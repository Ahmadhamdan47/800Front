import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { trigger, transition, query, style, stagger, animate } from '@angular/animations';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(-10px)' }),
        animate('0.3s ease-out', style({ opacity: 1, transform: 'translateY(0)' })),
      ]),
      transition(':leave', [
        animate('0.3s ease-in', style({ opacity: 0, transform: 'translateY(-10px)' })),
      ]),
    ]),
  ],
})
export class UserListComponent implements OnInit {
  users: any[] = [];
  searchUserId: number | null = null;
  isLoading = false;
  itemsPerPage = 3;
  currentPage = 1;
  totalPages = 2;
  showPagination = true;
  private userCache: { [userId: number]: any } = {};

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(): void {
    const url = 'https://reqres.in/api/users';

    if (this.users.length > 0) {
      return;
    }

    this.isLoading = true;

    this.http.get(url).pipe(
      tap((response: any) => {
        this.users = response.data;
        this.isLoading = false;
      })
    ).subscribe();
  }

  getUser(userId: number): any {
    if (this.userCache[userId]) {
      return this.userCache[userId];
    }

    const url = `https://reqres.in/api/users/${userId}`;

    return this.http.get(url).pipe(
      tap((response: any) => {
        this.userCache[userId] = response.data;
      })
    );
  }
  navigateToUserDetails(userId: number): void {
    this.router.navigate(['/user-details', userId]);
  }

  onSearch(): void {
    if (this.searchUserId === null) {
      return;
    }

    this.router.navigate(['/user-details', this.searchUserId]);
  }
  get displayedUsers() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.users.slice(startIndex, endIndex);
  }

  onPageChange(pageNumber: number): void {
    this.currentPage = pageNumber;
    this.showPagination = true;
  }
}