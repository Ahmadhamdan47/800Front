import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { trigger, transition, style, animate } from '@angular/animations';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate('300ms', style({ opacity: 0 }))
      ])
    ])
  ]

})
export class UserDetailsComponent implements OnInit {
  userId!: number;
  user: any; // To store user details

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Retrieve the user ID from the route parameters
    this.route.params.subscribe(params => {
      this.userId = +params['id']; // Convert to a number
      // Fetch user details based on the user ID
      this.fetchUserDetails();
    });
  }

  fetchUserDetails(): void {
    // Use the user ID to fetch user details from the API
    const url = `https://reqres.in/api/users/${this.userId}`;
    this.http.get(url).subscribe((response: any) => {
      this.user = response.data;
    });
  }

  goBack(): void {
    // Navigate back to the user list
    this.router.navigate(['/users']);
  }
}
